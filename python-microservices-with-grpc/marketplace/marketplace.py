import os

from flask import Flask, render_template
import grpc

app = Flask(__name__)

recommendations_host = "localhost"

recommendations_channel = grpc.insecure_channel(f"{recommendations_host}:50051")

recommendations_client = RecommendationsStub(recommendations_channel)


@app.route("/")
def render_homepage():
    #TODO
    return render_template(
        "homepage.html",
        recommendations=recommendations_response.recommendations,
    )
